﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Program_Exercise_2
{
   

    public partial class Form1 : Form
    {
        int x;
        int y;

        public Form1()
        {
            InitializeComponent();
        }

        private void Lb_shapes_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Invalidate the shapes for the Panel
            p_shapes.Invalidate();
        }

        private void P_shapes_Paint(object sender, PaintEventArgs e)
        {
        }

        private void Btn_show_Click(object sender, EventArgs e)
        {
            Graphics g = p_shapes.CreateGraphics();

            // Create the outline color and shape for the circle
            if (lb_shapes.SelectedIndex == 0)
            {
                Pen p = new Pen(Color.Black);               // Creates the outline color
                g.DrawEllipse(p, x + 25, y + 10, 100, 100); // Centers the circle in the Panel

                // if Radio Button Fill Color is selected
                if (rdb_fill.Checked)
                {
                    SolidBrush sb = new SolidBrush(Color.Red);   // fills the circle red
                    g.FillEllipse(sb, x + 25, y + 10, 100, 100); // displays the circle red
                    
                }
                // if Radio Button Outline shape is selected
                if (rdb_outline.Checked)
                {
                    Pen redPen = new Pen(Color.Red);                 // color changes to red
                    g.DrawEllipse(redPen, x + 25, y + 10, 100, 100); // Shows the new outlined color
                }
                // if check box display name is selected
                if (cb_shapeName.Checked)
                {
                    l_shapeName.Text = "Circle";
                }
                // if Check Box date is selected
                // Get current date
                if (cb_date.Checked)
                {
                    DateTime thisDay = DateTime.Now;
                    // Display the date
                    l_date.Text = thisDay.ToString();
                }
               
            }

            // Create the outline color and the shape for the Rectangle
            if (lb_shapes.SelectedIndex == 1)
            {
                Pen p = new Pen(Color.Black);                 // Creates the outline color
                g.DrawRectangle(p, x + 25, y + 10, 100, 100); // Centers the Rectangle in the Panel

                // if Radio Button Fill Color is selected
                if (rdb_fill.Checked)
                {
                    SolidBrush sb = new SolidBrush(Color.Blue);    // Fills the rectangle blue
                    g.FillRectangle(sb, x + 25, y + 10, 100, 100); // displays the rectangle blue
                }
                // if Radio Button Outline shape is selected
                if (rdb_outline.Checked)
                {
                    Pen bluePen = new Pen(Color.Blue);                // color changes to blue
                    g.DrawRectangle(bluePen, x + 25, y + 10, 100, 100); // Shows the new outlined color
                }
                // if check box display name is selected
                if (cb_shapeName.Checked)
                {
                    l_shapeName.Text = "Rectangle";
                }
                // if Check Box date is selected
                // Get current date
                if (cb_date.Checked)
                {
                    DateTime thisDay = DateTime.Now;
                    // Display the date
                    l_date.Text = thisDay.ToString();
                }
            }

            // Create the outline color and the shape for the triangle
            if (lb_shapes.SelectedIndex == 2)
            {
                Pen p = new Pen(Color.Black);
                // create points to create the triangle and to display
                Point[] pnt = new Point[3];

                pnt[0].X = 25;
                pnt[0].Y = 100;
                pnt[1].X = 125;
                pnt[1].Y = 100;
                pnt[2].X = 75;
                pnt[2].Y = 10;

                // Draw Polygon curve to screen to make the triangle
                g.DrawPolygon(p, pnt);

                // if Radio Button Fill Color is selected
                if (rdb_fill.Checked)
                {
                    SolidBrush sb = new SolidBrush(Color.Green); // fills the triangle green
                    g.FillPolygon(sb, pnt);                      // displays the triangle green
                } 
                // if Radio Button Outline shape is selected
                if (rdb_outline.Checked)
                {
                    Pen greenPen = new Pen(Color.Green);   // color changes to Greent
                    g.DrawPolygon(greenPen, pnt);          // Shows the new outlined color
                }
                // if check box display name is selected
                if (cb_shapeName.Checked)
                {
                    l_shapeName.Text = "Triangle";
                }
                // if Check Box date is selected
                // Get current date
                if (cb_date.Checked)
                {
                    DateTime thisDay = DateTime.Now;
                    // Display the date
                    l_date.Text = thisDay.ToString();
                }
            }

            // Create the outline color and the shape for the Hexagon
            if (lb_shapes.SelectedIndex == 3)
            {
                Pen p = new Pen(Color.Black);
                // create variables to match the width and heigth of the panel
                var x_0 = p_shapes.Width / 2;
                var y_0 = p_shapes.Height / 2;

                var hexagon = new PointF[6];

                var radius = 60;
                // A for loop to apply the Math and display the hexagon
                for (int a = 0; a < 6; a++)
                {
                    hexagon[a] = new PointF(x_0 + radius * (float)Math.Cos(a * 60 * Math.PI / 180f),
                                            y_0 + radius * (float)Math.Sin(a * 60 * Math.PI / 180f));
                }
                g.DrawPolygon(p, hexagon);

                // if Radio Button Fill Color is selected
                if (rdb_fill.Checked)
                {
                    SolidBrush sb = new SolidBrush(Color.Purple); // fills the hexagon purple
                    g.FillPolygon(sb, hexagon);                   // displays the hexagon purple
                }
                // if Radio Button Outline shape is selected
                if (rdb_outline.Checked)
                {
                    Pen purplePen = new Pen(Color.Purple);    // color changes to purple
                    g.DrawPolygon(purplePen, hexagon);        // Shows the new outlined color
                }
                // if check box display name is selected
                if (cb_shapeName.Checked)
                {
                    l_shapeName.Text = "Hexagon";
                }
                // if Check Box date is selected
                // Get current date
                if (cb_date.Checked)
                {
                    DateTime thisDay = DateTime.Now;
                    // Display the date
                    l_date.Text = thisDay.ToString();
                }
            }

        }

        private void Btn_clear_Click(object sender, EventArgs e)
        {
            // Clear the form from the panel, radio buttons, check buttons and label displays.
            p_shapes.Invalidate();
            rdb_fill.Checked = false;
            rdb_outline.Checked = false;
            cb_date.Checked = false;
            cb_shapeName.Checked = false;
            l_date.Text = "";
            l_shapeName.Text = "";
        }

        private void Btn_exit_Click(object sender, EventArgs e)
        {
            // Exit the form
            this.Close();
        }
    }
}

