﻿namespace Program_Exercise_2
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lb_shapes = new System.Windows.Forms.ListBox();
            this.p_shapes = new System.Windows.Forms.Panel();
            this.l_select = new System.Windows.Forms.Label();
            this.gb_color = new System.Windows.Forms.GroupBox();
            this.rdb_outline = new System.Windows.Forms.RadioButton();
            this.rdb_fill = new System.Windows.Forms.RadioButton();
            this.gb_display = new System.Windows.Forms.GroupBox();
            this.cb_date = new System.Windows.Forms.CheckBox();
            this.cb_shapeName = new System.Windows.Forms.CheckBox();
            this.btn_show = new System.Windows.Forms.Button();
            this.btn_clear = new System.Windows.Forms.Button();
            this.btn_exit = new System.Windows.Forms.Button();
            this.l_shapeName = new System.Windows.Forms.Label();
            this.l_date = new System.Windows.Forms.Label();
            this.gb_color.SuspendLayout();
            this.gb_display.SuspendLayout();
            this.SuspendLayout();
            // 
            // lb_shapes
            // 
            this.lb_shapes.FormattingEnabled = true;
            this.lb_shapes.ItemHeight = 16;
            this.lb_shapes.Items.AddRange(new object[] {
            "Circle",
            "Rectangle",
            "Triangle",
            "Hexagon"});
            this.lb_shapes.Location = new System.Drawing.Point(63, 87);
            this.lb_shapes.Name = "lb_shapes";
            this.lb_shapes.Size = new System.Drawing.Size(167, 68);
            this.lb_shapes.TabIndex = 0;
            this.lb_shapes.SelectedIndexChanged += new System.EventHandler(this.Lb_shapes_SelectedIndexChanged);
            // 
            // p_shapes
            // 
            this.p_shapes.BackColor = System.Drawing.Color.White;
            this.p_shapes.Location = new System.Drawing.Point(298, 87);
            this.p_shapes.Name = "p_shapes";
            this.p_shapes.Size = new System.Drawing.Size(200, 150);
            this.p_shapes.TabIndex = 1;
            this.p_shapes.Paint += new System.Windows.Forms.PaintEventHandler(this.P_shapes_Paint);
            // 
            // l_select
            // 
            this.l_select.AutoSize = true;
            this.l_select.Location = new System.Drawing.Point(60, 58);
            this.l_select.Name = "l_select";
            this.l_select.Size = new System.Drawing.Size(126, 17);
            this.l_select.TabIndex = 2;
            this.l_select.Text = "Select your shape:";
            // 
            // gb_color
            // 
            this.gb_color.BackColor = System.Drawing.Color.White;
            this.gb_color.Controls.Add(this.rdb_outline);
            this.gb_color.Controls.Add(this.rdb_fill);
            this.gb_color.Location = new System.Drawing.Point(44, 189);
            this.gb_color.Name = "gb_color";
            this.gb_color.Size = new System.Drawing.Size(200, 100);
            this.gb_color.TabIndex = 3;
            this.gb_color.TabStop = false;
            this.gb_color.Text = "Pick a Fill Choice:";
            // 
            // rdb_outline
            // 
            this.rdb_outline.AutoSize = true;
            this.rdb_outline.Location = new System.Drawing.Point(22, 62);
            this.rdb_outline.Name = "rdb_outline";
            this.rdb_outline.Size = new System.Drawing.Size(119, 21);
            this.rdb_outline.TabIndex = 1;
            this.rdb_outline.TabStop = true;
            this.rdb_outline.Text = "Outline Shape";
            this.rdb_outline.UseVisualStyleBackColor = true;
            // 
            // rdb_fill
            // 
            this.rdb_fill.AutoSize = true;
            this.rdb_fill.Location = new System.Drawing.Point(22, 34);
            this.rdb_fill.Name = "rdb_fill";
            this.rdb_fill.Size = new System.Drawing.Size(91, 21);
            this.rdb_fill.TabIndex = 0;
            this.rdb_fill.TabStop = true;
            this.rdb_fill.Text = "Fill Shape";
            this.rdb_fill.UseVisualStyleBackColor = true;
            // 
            // gb_display
            // 
            this.gb_display.Controls.Add(this.cb_date);
            this.gb_display.Controls.Add(this.cb_shapeName);
            this.gb_display.Location = new System.Drawing.Point(44, 321);
            this.gb_display.Name = "gb_display";
            this.gb_display.Size = new System.Drawing.Size(200, 100);
            this.gb_display.TabIndex = 4;
            this.gb_display.TabStop = false;
            this.gb_display.Text = "Display Name and Date:";
            // 
            // cb_date
            // 
            this.cb_date.AutoSize = true;
            this.cb_date.Location = new System.Drawing.Point(22, 59);
            this.cb_date.Name = "cb_date";
            this.cb_date.Size = new System.Drawing.Size(60, 21);
            this.cb_date.TabIndex = 1;
            this.cb_date.Text = "Date";
            this.cb_date.UseVisualStyleBackColor = true;
            // 
            // cb_shapeName
            // 
            this.cb_shapeName.AutoSize = true;
            this.cb_shapeName.Location = new System.Drawing.Point(22, 31);
            this.cb_shapeName.Name = "cb_shapeName";
            this.cb_shapeName.Size = new System.Drawing.Size(119, 21);
            this.cb_shapeName.TabIndex = 0;
            this.cb_shapeName.Text = "Shapes Name";
            this.cb_shapeName.UseVisualStyleBackColor = true;
            // 
            // btn_show
            // 
            this.btn_show.Location = new System.Drawing.Point(277, 342);
            this.btn_show.Name = "btn_show";
            this.btn_show.Size = new System.Drawing.Size(75, 23);
            this.btn_show.TabIndex = 5;
            this.btn_show.Text = "Show";
            this.btn_show.UseVisualStyleBackColor = true;
            this.btn_show.Click += new System.EventHandler(this.Btn_show_Click);
            // 
            // btn_clear
            // 
            this.btn_clear.Location = new System.Drawing.Point(358, 342);
            this.btn_clear.Name = "btn_clear";
            this.btn_clear.Size = new System.Drawing.Size(75, 23);
            this.btn_clear.TabIndex = 6;
            this.btn_clear.Text = "Clear";
            this.btn_clear.UseVisualStyleBackColor = true;
            this.btn_clear.Click += new System.EventHandler(this.Btn_clear_Click);
            // 
            // btn_exit
            // 
            this.btn_exit.Location = new System.Drawing.Point(439, 342);
            this.btn_exit.Name = "btn_exit";
            this.btn_exit.Size = new System.Drawing.Size(75, 23);
            this.btn_exit.TabIndex = 7;
            this.btn_exit.Text = "Exit";
            this.btn_exit.UseVisualStyleBackColor = true;
            this.btn_exit.Click += new System.EventHandler(this.Btn_exit_Click);
            // 
            // l_shapeName
            // 
            this.l_shapeName.BackColor = System.Drawing.SystemColors.Control;
            this.l_shapeName.Location = new System.Drawing.Point(316, 253);
            this.l_shapeName.Name = "l_shapeName";
            this.l_shapeName.Size = new System.Drawing.Size(165, 23);
            this.l_shapeName.TabIndex = 8;
            this.l_shapeName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // l_date
            // 
            this.l_date.BackColor = System.Drawing.SystemColors.Control;
            this.l_date.Location = new System.Drawing.Point(316, 289);
            this.l_date.Name = "l_date";
            this.l_date.Size = new System.Drawing.Size(165, 23);
            this.l_date.TabIndex = 9;
            this.l_date.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ClientSize = new System.Drawing.Size(632, 450);
            this.Controls.Add(this.l_date);
            this.Controls.Add(this.l_shapeName);
            this.Controls.Add(this.btn_exit);
            this.Controls.Add(this.btn_clear);
            this.Controls.Add(this.btn_show);
            this.Controls.Add(this.gb_display);
            this.Controls.Add(this.gb_color);
            this.Controls.Add(this.l_select);
            this.Controls.Add(this.p_shapes);
            this.Controls.Add(this.lb_shapes);
            this.DoubleBuffered = true;
            this.Name = "Form1";
            this.Text = "Programming Exercise 2";
            this.gb_color.ResumeLayout(false);
            this.gb_color.PerformLayout();
            this.gb_display.ResumeLayout(false);
            this.gb_display.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox lb_shapes;
        private System.Windows.Forms.Panel p_shapes;
        private System.Windows.Forms.Label l_select;
        private System.Windows.Forms.GroupBox gb_color;
        private System.Windows.Forms.RadioButton rdb_outline;
        private System.Windows.Forms.RadioButton rdb_fill;
        private System.Windows.Forms.GroupBox gb_display;
        private System.Windows.Forms.CheckBox cb_date;
        private System.Windows.Forms.CheckBox cb_shapeName;
        private System.Windows.Forms.Button btn_show;
        private System.Windows.Forms.Button btn_clear;
        private System.Windows.Forms.Button btn_exit;
        private System.Windows.Forms.Label l_shapeName;
        private System.Windows.Forms.Label l_date;
    }
}

